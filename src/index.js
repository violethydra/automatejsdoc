/**
 * @function veganizeManyFoods
 * @param {Array<Food>} arrayOfFoods Your shopping list
 * @returns {Array<Food>} the veganized version of our shopping list
 */
const veganizeManyFoods = (arrayOfFoods) => {
  return arrayOfFoods.map((food) => {
    if (food.type === "veggie") console.log("this is a vegetable!");
    // VS Code automatically check if type is one of "meat", "veggie" or "other"
    // it will send an error because 'cheese' is not a food type
    if (food.type === "cheese") food.name = getVeganFood("cheese");

    if (food.type === "meat") {
      // VS Code send an error because the food object is not a string
      food.name = getVeganFood(food);
    }
    return food;
  });
};

/**
 * @function shoppingList
 * @param  {type} const shoppingList {description}
 * @return {type} {description}
 */
const shoppingList = [
  {
    name: "steak",
    type: "meat",
    name: "Chocolate Bar",
    type: "snack",
    name: "Artichoke",
    type: "veggie",
  },
];

/**
 * @typedef {Object} Food
 * @property {string} name - What the food should be called
 * @property {('meat' | 'veggie' | 'other')} type - The food's type
 */
const veganShoppingList = veganizeManyFoods(shoppingList);
console.log('veganShoppingList', veganShoppingList)

// in an idea world this should equal
// [
//   { name: 'vegan steak', type: 'meat' },
//   { name: 'Chocolate Bar', type: 'snack' },
//   { name: 'Artichoke', type: 'veggie' }
// ]

export default veganShoppingList