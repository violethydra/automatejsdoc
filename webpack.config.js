const nodeExternals = require('webpack-node-externals');
const { resolve } = require('path');
const wpDev = require('./webpack/webpack.development.js');
const wpProd = require('./webpack/webpack.production.js');

let nodeENV = process.env.NODE_ENV || 'development';


console.log('\n>> wp config file ::', nodeENV);

if (nodeENV !== 'production') {
  if (nodeENV !== 'development') {
    const err = text => console.log(String(new Error(text)));
    err('Warning, use PRODUCTION mode in webpack config\n');
    nodeENV = 'error!';
  }
}

const obj = nodeENV !== 'production' ? wpDev : wpProd;


console.log('[LOG] nodeENV', `<${typeof nodeENV}>`, nodeENV);
console.log(nodeENV !== 'production', 'boolean')

module.exports = {
  ...obj,
  name: nodeENV,
  entry: ['./src'],
  output: {
    path: resolve(process.cwd(), 'dist'),
    filename: 'index.js',
    publicPath: '/',
    hotUpdateChunkFilename: '.hot/[id].[hash].hot-update.js',
    hotUpdateMainFilename: '.hot/[hash].hot-update.json',
  },
  target: 'node',
  // watch: true,
  watch: nodeENV !== 'production',
  mode: String(nodeENV),
  node: { __dirname: false, __filename: false },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: { loader: 'babel-loader' },
      },
    ],
  },
  externals: [nodeExternals()],
};
