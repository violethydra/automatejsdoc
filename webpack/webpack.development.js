const webpack = require('webpack');
const Dotenv = require('dotenv-webpack');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin');

module.exports = {
  devtool: 'eval-cheap-source-map',
  optimization: {
    // splitChunks: { chunks: 'all' },
    chunkIds: 'total-size',
    moduleIds: 'size',
  },

  plugins: [
    new CleanWebpackPlugin(),
    new FriendlyErrorsWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.ProgressPlugin(),
    new Dotenv({ path: './.access/.env' }),
  ],
};
